/* ========================================================================
 * Copyright 2012 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.spycamera;

import java.lang.Thread.UncaughtExceptionHandler;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Surface;

import com.jwork.spycamera.utility.CrashHandler;
import com.jwork.spycamera.utility.Factory;
import com.jwork.spycamera.utility.LogUtility;

/**
 * @author Jimmy Halim
 */
public class SpyCamActivity extends FragmentActivity {
	
	private LogUtility log;
	private MainFragment fragment;
	private UncaughtExceptionHandler defaultUEH;
	private CrashHandler crashHandler;
	private int defaultOrientation;
	
	public SpyCamActivity() {
		Factory.reset();
		log = LogUtility.getInstance();
		log.v(this, "constructor()");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		log.v(this, "onCreate()");
		super.onCreate(savedInstanceState);
		
		defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		crashHandler = CrashHandler.getInstance(this, defaultUEH);
		Thread.setDefaultUncaughtExceptionHandler(crashHandler);

		setContentView(R.layout.activity_main);
		fragment = (MainFragment)getSupportFragmentManager().findFragmentById(R.id.fragmentMain);
		
		getDefaultOrientation();
	}


	private void getDefaultOrientation() {
		int rotation = getWindowManager().getDefaultDisplay().getRotation();
		switch (rotation) {
		case Surface.ROTATION_0: 
			defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
			break;
		case Surface.ROTATION_90: 
			defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE;
			break;
		case Surface.ROTATION_180: 
			if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
				defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
			} else {
				defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				setRequestedOrientation(defaultOrientation);
			}
			break;
		case Surface.ROTATION_270: 
			if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
				defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
			} else {
				defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				setRequestedOrientation(defaultOrientation);
			}
			break;
		}
	}

	@SuppressLint("NewApi")
	@Override
	protected void onResume() {
		log.v(this, "onResume()");
		super.onResume();
	}

	@Override
	protected void onPause() {
		log.v(this, "onPause()");
		super.onPause();
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		if (defaultUEH!=null) {
			Thread.setDefaultUncaughtExceptionHandler(defaultUEH);
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		log.d(this, "onConfigurationChanged");
		setRequestedOrientation(defaultOrientation);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		log.v(this, "onKeyDown(keycode:"+keyCode+")");
		if (fragment.onKeyDown(keyCode, event)) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
}
