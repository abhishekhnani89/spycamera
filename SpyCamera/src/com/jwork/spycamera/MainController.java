/* ========================================================================
 * Copyright 2012 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.spycamera;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.OnZoomChangeListener;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.ScaleGestureDetector;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.jwork.spycamera.model.FailedProcessData;
import com.jwork.spycamera.utility.ConfigurationUtility;
import com.jwork.spycamera.utility.CrashHandler;
import com.jwork.spycamera.utility.LogUtility;
import com.jwork.spycamera.utility.Utility;

/**
 * @author Jimmy Halim
 */
public class MainController implements OnZoomChangeListener, PreviewCallback, AutoFocusCallback
	, PictureCallback, ShutterCallback {

	private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");

	public static final int STATE_IDLE = 0;
	public static final int STATE_IMAGE_SINGLE = 1;
	public static final int STATE_IMAGE_AUTO = 2;
	public static final int STATE_IMAGE_FACE = 3;
	public static final int STATE_VIDEO_RECORDING = 4;
	private int state = STATE_IDLE;

	private LogUtility log;
	private Activity activity;
	private Handler handler;
	private Camera camera;
	private SurfaceHolder shPreview;
	private ConfigurationUtility config;

	private boolean isHolderReady = false;
	private boolean isCameraConfigure = false;
	private boolean isBlackScreen = false;
	private int cameraId;
	private long lastCapture = 0;
	private long lastStartAutofocus = 0;
	private long avgAutofocusTime = -1;
	private String[][] cameraPreviewSizes;
	private String[][] videoQualities;
	private boolean crashReport = false;
	private int zoomCurrent = 0;
	private int defaultOrientation;
	private int currentCamcorderProfileId;
	private Parameters cameraParameters;
	private boolean isImageHighRes;
	private byte[] bSnapShot;
	private Camera.Size previewSize;
	private Camera.Size previewSizeHighest;
	private Camera.Size captureSize;
	private Camera.Size captureSizeHighest;
	private MediaRecorder recorder;
	private CamcorderProfile currentCamcorderProfile = null;
	private boolean zoomRunning;
	private boolean isZoomErrorHaveDisplayed = false;
	private int tempPreviewWidth = 1;
	
	private boolean isTakingPicture = false;
	private boolean isUIDisplayed = false;

	private int ringerMode;

	private SurfaceView svPreview;

	public MainController(Activity activity, Handler handler) {
		this.activity = activity;
		this.handler = handler;
		this.log = LogUtility.getInstance();
		this.config = ConfigurationUtility.getInstance(activity);
	}

	public void initData() {
		log.v(this,"initData()");

		crashReport = checkPreviousCrash();

		if (!crashReport) {
			Message msg =  new Message();
			msg.what = MainHandler.WHAT_SET_PREVIEW_IMAGE;
			msg.arg1 = config.getPreviewWidthSize();
			msg.arg2 = msg.arg1;
			handler.sendMessage(msg);
		}
		
		if (!Utility.showChangelogNew(true, activity, (camera==null)) && config.isStartupBlackMode()) {
			switchBlackScreen();
		}
	}

	@SuppressLint("NewApi")
	public void startCamera(SurfaceView svPreview) {
		log.v(this,"startCamera()");
		this.svPreview = svPreview;
		if (!crashReport) {
			cameraId = 0;
			try {
				if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.FROYO) {
					camera = Camera.open();
					cameraPreviewSizes = new String[1][];
					videoQualities = new String[1][];
					Message msg =  new Message();
					msg.what = MainHandler.WHAT_HIDE_COMPONENT;
					msg.arg1 = R.id.btnSwitchCam;
					handler.sendMessage(msg);
				} else {
					int total = Camera.getNumberOfCameras();
					if (total>2) {
						total = 2;
					} else if (total==1) {
						Message msg =  new Message();
						msg.what = MainHandler.WHAT_HIDE_COMPONENT;
						msg.arg1 = R.id.btnSwitchCam;
						handler.sendMessage(msg);
					}

					cameraPreviewSizes = new String[total][];
					videoQualities = new String[total][];

					cameraId = config.getCurrentCamera();

					if (total>1) {
						int anotherCameraId = (cameraId==0)?1:0;
						String temp = config.getCameraPreviewSizes(anotherCameraId);
						if (temp==null) {
							camera = Camera.open(anotherCameraId);
							StringBuffer data = new StringBuffer();
							cameraPreviewSizes[anotherCameraId] = Utility.cameraSizeSupport(camera.getParameters(), data);
							camera.release();
							config.setCameraPreviewSizes(anotherCameraId, data.toString());
						} else {
							cameraPreviewSizes[anotherCameraId]=temp.split("#");
						}
						temp = config.getVideoQualityList(anotherCameraId);
						if (temp==null) {
							StringBuffer data = new StringBuffer();
							videoQualities[anotherCameraId] = Utility.camcorderProfileSupport(anotherCameraId, data);
							config.setVideoQualityList(anotherCameraId, data.toString());
						} else {
							videoQualities[anotherCameraId] = temp.split("#");
						}
					}
					camera = Camera.open(cameraId);
				}
				cameraParameters = camera.getParameters();

				String temp = config.getCameraPreviewSizes(cameraId);
				if (temp==null) {
					// Get preview sizes
					StringBuffer data = new StringBuffer();
					cameraPreviewSizes[cameraId] = Utility.cameraSizeSupport(camera.getParameters(), data);
					config.setCameraPreviewSizes(cameraId, data.toString());
				} else {
					cameraPreviewSizes[cameraId] = temp.split("#");
				}
				temp = config.getVideoQualityList(cameraId);
				if (temp==null) {
					StringBuffer data = new StringBuffer();
					videoQualities[cameraId] = Utility.camcorderProfileSupport(cameraId, data);
					config.setVideoQualityList(cameraId, data.toString());
				} else {
					videoQualities[cameraId] = temp.split("#");
				}


				String configSize = config.getImageCaptureSize(cameraId);
				if (configSize!=null) {
					if (configSize.endsWith("*")) {
						isImageHighRes = true;
					} else {
						isImageHighRes = false;
					}
				} else {
					if (!config.isHaveOutOfMemoryIssue()) {
						isImageHighRes = false;
					} else {
						isImageHighRes = true;
					}
				}

				// Analyze preview/capture size
				int highestPreview = 0;
				int highestCapture = 0;
				this.previewSize = null;
				this.previewSizeHighest = null;
				this.captureSize = null;
				this.captureSizeHighest = null;
				for (String previewSize : cameraPreviewSizes[cameraId]) {
					int w = 0;
					int h = 0;
					String size = null;
					if (previewSize.endsWith("*")) {
						size = previewSize.substring(0, previewSize.length()-1);
					} else {
						size = previewSize;
					}
					String[] temp2 = size.split("x");
					try {
						w = Integer.parseInt(temp2[0]);
					} catch (NumberFormatException e) {}
					try {
						h = Integer.parseInt(temp2[1]);
					} catch (NumberFormatException e) {}

					if (previewSize.endsWith("*") && (highestCapture<w*h)) {
						captureSizeHighest = camera.new Size(w, h);
						highestCapture = w*h;
					} else if (!previewSize.endsWith("*") && (highestPreview<w*h)) {
						previewSizeHighest = camera.new Size(w, h);
						highestPreview = w*h;
					}

					if (configSize!=null && previewSize.equals(configSize)) {
						if (isImageHighRes) {
							this.captureSize = camera.new Size(w, h);
							this.previewSize = null;
						} else {
							this.previewSize = camera.new Size(w, h);
							this.captureSize = null;
						}
					}
				}

				if (previewSize==null) {
					previewSize = previewSizeHighest;
				}

				if (isImageHighRes && this.captureSize==null) {
					this.captureSize = captureSizeHighest;
				}

				if (isImageHighRes) {
					config.setImageCaptureSize(cameraId, captureSize.width+"x"+captureSize.height+"*");
				} else {
					config.setImageCaptureSize(cameraId, previewSize.width+"x"+previewSize.height);			
				}

				refreshImagePreviewSize();

				if (shPreview!=null) {
					try {
						camera.setPreviewDisplay(shPreview);
					} catch (IOException e) {
						log.w(this, e);
					}
				}

				//Video Quality
				int videoprofile = config.getVideoRecordingQuality(cameraId);
				try {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
						log.d(this, "Current camcorder profile : " + videoprofile);
						currentCamcorderProfile = CamcorderProfile.get(cameraId, videoprofile);
						currentCamcorderProfileId = videoprofile;
					}
				} catch (IllegalArgumentException e) {
					log.d(this, "Fail getting camcorder profile, change using : " + CamcorderProfile.QUALITY_LOW);
					try {
						currentCamcorderProfile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_LOW);
						currentCamcorderProfileId = CamcorderProfile.QUALITY_LOW;
					} catch (IllegalArgumentException e2) {
						log.d(this, "Still fail getting camcorder profile, disabling video recording");
						showToast(true, Toast.LENGTH_LONG, "Unable to initialize video recording, disabling the feature");
						Message msg =  new Message();
						msg.what = MainHandler.WHAT_DISABLE_COMPONENT;
						msg.arg1 = R.id.btnVideo;
						handler.sendMessage(msg);
					}
				}
			} catch (RuntimeException re) {
				log.w(this, re);
				crashReport = true;
				if (re.getMessage().toLowerCase().contains("connect")) {
					config.clear(false);
					Message msg = new Message();
					msg.what = MainHandler.WHAT_SHOW_FAILED_PROCESS;
					msg.obj = new FailedProcessData(re
							, "Failed initializing camera. Please try to reboot your phone manually and run the application again."
							, "After reboot still error"
							, "OK", true, null);
					handler.sendMessage(msg);
				} else if (re.getMessage().toLowerCase().contains("getparameters")) {
					config.clear(false);
					Message msg = new Message();
					msg.what = MainHandler.WHAT_SHOW_FAILED_PROCESS;
					msg.obj = new FailedProcessData(re
							, "Failed getting camera parameters. Sending crash report will help me fix it."
							, "Send Report"
							, "I've sent it", true, null);
					handler.sendMessage(msg);
				} else {
					throw re;
				}
			}

			if (isCameraConfigure) {
				startCameraPreview(shPreview);
			} else if (isHolderReady) {
				configureCamera(shPreview);
			}
		}
	}

	private void refreshImagePreviewSize() {
		Message msg =  new Message();
		msg.what = MainHandler.WHAT_SET_PREVIEW_IMAGE;
		msg.arg1 = config.getPreviewWidthSize();
		msg.arg2 = calculatePreviewHeight(msg.arg1);
		if (msg.arg2<1) {
			msg.arg2 = 1;
		}
		handler.sendMessage(msg);
	}

	private int calculatePreviewHeight(int width) {
		int height = -1;
		float ratio = 1;
		if (isImageHighRes) {
			if (captureSize==null) {
				log.v(this, "calculatePreviewHeight(width:"+width+"):" + width);
				return width;
			}
			ratio = (float)captureSize.height/(float)captureSize.width;
		} else {
			if (previewSize==null) {
				log.v(this, "calculatePreviewHeight(width:"+width+"):" + width);
				return width;
			}
			ratio = (float)previewSize.height/(float)previewSize.width; 
		}
		if (defaultOrientation==ActivityInfo.SCREEN_ORIENTATION_PORTRAIT || defaultOrientation==ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
			ratio = 1/ratio;
		}
		height = (int)(width*ratio);
		log.v(this, "calculatePreviewHeight(width:"+width+"):" + height);
		return height;
	}

	public void showToast(boolean force, int length, Object message) {
		if (!isUIDisplayed) {
			return;
		}
		Message msg =  new Message();
		msg.what = MainHandler.WHAT_SHOW_TOAST;
		msg.arg1 = force?1:0;
		msg.arg2 = length;
		msg.obj = message;
		handler.sendMessage(msg);
	}

	public synchronized void configureCamera(SurfaceHolder holder) {
		log.v(this,"configureCamera()");
		isHolderReady = true;
		shPreview = holder;
		if (camera==null) {
			log.w(this, "configureCamera: camera is null");
			return;
		}
		if (!isCameraConfigure) {
			if (crashReport) {
				return;
			}
			int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
			int degrees = 0;
			switch (rotation) {
			case Surface.ROTATION_0: 
				degrees = 90; 
				defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				break;
			case Surface.ROTATION_90: 
				degrees = 0; 
				defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE;
				break;
			case Surface.ROTATION_180: 
				degrees = 270;
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
					defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				} else {
					defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
					activity.setRequestedOrientation(defaultOrientation);
				}
				break;
			case Surface.ROTATION_270: 
				degrees = 180; 
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
					defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				} else {
					defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
					activity.setRequestedOrientation(defaultOrientation);
				}
				break;
			}
			try {
				refreshImagePreviewSize();
				camera.setDisplayOrientation(degrees);
			} catch (RuntimeException re) {
				if (!config.getBoolean(ConfigurationUtility.PREFS_ERRORREPORT_SETDISPLAYORIENTATION, false)) {
					Message msg = new Message();
					msg.what = MainHandler.WHAT_SHOW_FAILED_PROCESS;
					msg.obj = new FailedProcessData(re
							, "Failed setting preview orientation. Sending crash report will help me fix it."
							, "Send Report"
							, "OK", false, ConfigurationUtility.PREFS_ERRORREPORT_SETDISPLAYORIENTATION);
					handler.sendMessage(msg);
				} else {
					showToast(true, Toast.LENGTH_LONG, "Failed setting preview orientation.");
				}
			}

			if (isImageHighRes) {
				cameraParameters.setPictureSize(captureSize.width, captureSize.height);
				cameraParameters.setPictureFormat(ImageFormat.JPEG);
				cameraParameters.setRotation((cameraId==0)?90:270);
			}
			log.i(this, "previewSize.width : " + previewSize.width + "x" + previewSize.height);
			cameraParameters.setPreviewSize(previewSize.width, previewSize.height);
			cameraParameters.setPreviewFormat(ImageFormat.NV21);
			if (cameraParameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO))
			{
				cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
			} else {
				log.d(this, "Focus mode auto not supported : " + Arrays.toString(cameraParameters.getSupportedFocusModes().toArray()));
			}
			camera.setParameters(cameraParameters);

			startCameraPreview(shPreview);
			isCameraConfigure = true;
		} else {
			log.w(this, "Camera already configured");
			refreshImagePreviewSize();
		}
	}

	public void startCameraPreview(SurfaceHolder holder) {
		log.v(this,"startCameraPreview()");
		try {
			log.i(this, "Starting preview");
			camera.setZoomChangeListener(this);
			camera.setPreviewDisplay(holder);
			camera.startPreview();
			camera.setPreviewCallback(this);
		} catch (IOException e) {
			showToast(true, Toast.LENGTH_LONG, "Failed initializing camera preview");
			log.e(this, e);
		} catch (RuntimeException re) {
			if (re.getMessage().toLowerCase().contains("startpreview")) {
				config.clear(false);
				Message msg = new Message();
				msg.what = MainHandler.WHAT_SHOW_FAILED_PROCESS;
				msg.obj = new FailedProcessData(re
						, "Failed starting camera preview. Sending crash report will help me fix it."
						, "Send Report"
						, "I've sent it", false, null);
				handler.sendMessage(msg);
			} else {
				throw re;
			}
		}
	}

	public void stopCamera() {
		log.v(this,"stopCamera()");
		if (!crashReport) {
			if (state==STATE_VIDEO_RECORDING) {
				try {
					stopVideoRecording();
				} catch (RuntimeException e) {
					log.w(this, e);
				}
			}

			try {
				camera.cancelAutoFocus();
			} catch (Throwable e){
			}
			camera.setPreviewCallback(null);
			//		camera.stopPreview();
			try {
				camera.release();
				camera = null;
				isCameraConfigure = false;
			} catch (Throwable e) {
				log.w(this, e);
			}
			setState(STATE_IDLE);
		}
	}
	
	private void stopVideoRecording() {
		if (recorder!=null) {
			showToast(false, Toast.LENGTH_SHORT, R.string.message_stopVideo);

			setState(STATE_IDLE);
			try {
				recorder.stop();
				recorder.reset();   
				recorder.release();
				camera.lock();
			} catch (RuntimeException re) {
				showToast(false, Toast.LENGTH_SHORT, "Encounter error on stopping video recording");
				log.w(this, re);
			}
			try {
				camera.reconnect();
			} catch (IOException e) {
				log.w(this, e);
			}
		}
	}


	public void switchCamera() {
		if (cameraPreviewSizes==null || cameraPreviewSizes.length<=1) {
			return;
		}
		int activeCamera = config.getCurrentCamera();
		if (activeCamera==0) {
			activeCamera = 1;
		} else {
			activeCamera = 0;
		}
		config.setCurrentCamera(activeCamera);
		stopCamera();
		startCamera(svPreview);
	}

	public void switchBlackScreen() {
		log.v(this, "onKeyDown()|isBlackScreen:"+isBlackScreen);
		if (!isBlackScreen) {
			Message msg = new Message();
			msg.what = MainHandler.WHAT_SHOW_COMPONENT;
			msg.arg1 = R.id.blackLayout;
			handler.sendMessage(msg);
			
			showToast(true, Toast.LENGTH_SHORT, activity.getString(R.string.hint_blackmode));

			tempPreviewWidth = config.getPreviewWidthSize();
			config.setPreviewWidthSize(1);
			refreshImagePreviewSize();
		} else {
			Message msg = new Message();
			msg.what = MainHandler.WHAT_HIDE_COMPONENT;
			msg.arg1 = R.id.blackLayout;
			handler.sendMessage(msg);
			config.setPreviewWidthSize(tempPreviewWidth);
			refreshImagePreviewSize();
		}
		isBlackScreen=!isBlackScreen;
	}

	public void imageCapture() {
		log.v(this, "imageCapture()");
		if (state==STATE_IDLE) {
			setState(STATE_IMAGE_SINGLE);
			startAutoFocus();
		}
	}

	private void startAutoFocus() {
		log.v(this, "startAutoFocus()");
		if (isCameraConfigure) {
			lastStartAutofocus = System.currentTimeMillis();
			if (config.isUseAutoFocus()) {
				try {
					log.d(this, "current autofocus mode: "+cameraParameters.getFocusMode());
					camera.autoFocus(this);
				} catch (RuntimeException re) {
					log.e(this, re);
					onAutoFocus(true, camera);
				}
			} else {
				log.d(this, "Canceling autofocus, calling onAutoFocus directly"); 
				onAutoFocus(true, camera);
			}
		}
	}

	public void autoImageCapture() {
		log.v(this, "autoImageCapture()");
		if (state==STATE_IDLE) {
			setState(STATE_IMAGE_AUTO);
			showToast(false, Toast.LENGTH_SHORT, activity.getString(R.string.message_startAuto,(config.getAutoCaptureDelay()/1000)));
			startAutoFocus();
		} else if (state==STATE_IMAGE_AUTO) {
			setState(STATE_IDLE);
			showToast(false, Toast.LENGTH_SHORT, R.string.message_stopAuto);
		}
	}

	public void openSetting() {
		Intent intent = new Intent(activity, SpyCamPrefsActivity.class);
		
		if (cameraPreviewSizes[0]!=null) {
			intent.putExtra("cameraPreviewSizes0", cameraPreviewSizes[0]);
		}
		if (cameraPreviewSizes.length>1 && cameraPreviewSizes[1]!=null) {
			intent.putExtra("cameraPreviewSizes1", cameraPreviewSizes[1]);
		}

		intent.putExtra("cameraNumber", cameraPreviewSizes.length);

		activity.startActivity(intent);
	}

	@TargetApi(14)
	public void autoFaceCapture() {
		log.v(this, "autoFaceCapture()");
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			showToast(true, Toast.LENGTH_SHORT, "Facedetection is only supported for Android 4.0 or newer");
			return;
		}
		if (camera==null) {
			showToast(true, Toast.LENGTH_SHORT, "Camera is not initialized yet");
			return;
		}
		if (state==STATE_IDLE) {
			setState(STATE_IMAGE_FACE);
			camera.setFaceDetectionListener(new FaceDetectionListener() {
				@Override
				public void onFaceDetection(Face[] faces, Camera camera) {
					long delay = config.getAutoCaptureDelay();
					if (System.currentTimeMillis()-lastCapture>delay-avgAutofocusTime && faces.length>0) {
						log.d(this, "Face detected : " + faces.length);
						startAutoFocus();
					}
				}
			});
			if (cameraParameters.getMaxNumDetectedFaces()>0) {
				camera.startFaceDetection();
			}
			showToast(false, Toast.LENGTH_SHORT, R.string.message_startFace);
		} else if (state==STATE_IMAGE_FACE) {
			setState(STATE_IDLE);
			camera.setFaceDetectionListener(null);
			if (cameraParameters.getMaxNumDetectedFaces()>0) {
				camera.stopFaceDetection();
			}
			showToast(false, Toast.LENGTH_SHORT, R.string.message_stopFace);
		}
	}

	public void videoRecording() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
			showToast(true, Toast.LENGTH_SHORT, "Video recording is only supported for Android 2.3 or newer");
			return;
		}
		if (!config.isDisplayedVideoExperimentalNotice()) {
			Message msg = new Message();
			msg.what = MainHandler.WHAT_SHOW_VIDEO_RECORDING_EXPERIMENTAL_NOTICE;
			handler.sendMessage(msg);
			return;
		}
		if (state==STATE_IDLE) {
			File directory = new File(config.getSavingPath());
			if (!directory.exists()) {
				directory.mkdir();
			}
			String outputFile = null;
			try {
				try {
					camera.unlock();
				} catch (Throwable re) {
					log.w(this, re);
				}
				recorder = new MediaRecorder();
				recorder.setCamera(camera); // start failed: -19
				recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
				recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

				recorder.setProfile(currentCamcorderProfile);

				outputFile = directory.getAbsolutePath()+ "/SpyVideo_"+ SDF.format(new Date())+".mp4";
				recorder.setOutputFile(outputFile);
				recorder.setPreviewDisplay(shPreview.getSurface());
				recorder.prepare();
				recorder.start();   // Recording is now started

				addFileToMediaScanner(new File(outputFile));

				lastCapture = 0;
				showToast(false, Toast.LENGTH_SHORT, activity.getString(R.string.message_startVideo, directory.getAbsolutePath()+ "/SpyVideo_"+ SDF.format(new Date())+".mp4"));
				setState(STATE_VIDEO_RECORDING);
			} catch (IllegalStateException e) {
				log.w(this, e);
				showToast(true, Toast.LENGTH_SHORT, "Failed starting video recording");
				if (recorder!=null) {
					recorder.reset();   
					recorder.release();
				}
				try {
					camera.lock();
					camera.reconnect();
				} catch (Throwable e1) {}
				Message msg = new Message();
				msg.what = MainHandler.WHAT_SHOW_FAILED_PROCESS;
				msg.obj = new FailedProcessData(e
						, "Failed starting video recording.\nThis feature is still experimental.\nYou can try other quality setting.\nReporting it will help me solve issues"
						, "Report"
						, "OK", false, null);
				handler.sendMessage(msg);
				
				try {
					File failedFile = new File(outputFile);
					if (failedFile.exists()) {
						failedFile.delete();
					}
				} catch (Exception e2) {}
				
			} catch (IOException e) {
				log.w(this, e);
				showToast(true, Toast.LENGTH_SHORT, "Failed starting video recording");
				if (recorder!=null) {
					recorder.reset();
					recorder.release();
				}
				try {
					camera.lock();
					camera.reconnect();
				} catch (Throwable e1) {}
				Message msg = new Message();
				msg.what = MainHandler.WHAT_SHOW_FAILED_PROCESS;
				msg.obj = new FailedProcessData(e
						, "Failed starting video recording.\nThis feature is still experimental.\nYou can try other quality setting.\nReporting it will help me solve issues"
						, "Report"
						, "OK", false, null);
				handler.sendMessage(msg);
				try {
					File failedFile = new File(outputFile);
					if (failedFile.exists()) {
						failedFile.delete();
					}
				} catch (Exception e2) {}
			} catch (RuntimeException e) {
				log.w(this, e);
				showToast(true, Toast.LENGTH_SHORT, "Failed starting video recording");
				if (recorder!=null) {
					recorder.reset();  
					recorder.release();
				}
				try {
					camera.lock();
					camera.reconnect();
				} catch (Throwable e1) {}
				Message msg = new Message();
				msg.what = MainHandler.WHAT_SHOW_FAILED_PROCESS;
				msg.obj = new FailedProcessData(e
						, "Failed starting video recording.\nThis feature is still experimental.\nYou can try other quality setting.\nReporting it will help me solve issues"
						, "Report"
						, "OK", false, null);
				handler.sendMessage(msg);
				try {
					File failedFile = new File(outputFile);
					if (failedFile.exists()) {
						failedFile.delete();
					}
				} catch (Exception e2) {}
			} finally {
//				unmute();
			}
		} else {
			stopVideoRecording();
		}

	}

	public void incPreviewSize(int maxWidth) {
		int w = config.getPreviewWidthSize()+10;
		int max = maxWidth*3/4;
		if (w>max) {
			w = max;
		}
		config.setPreviewWidthSize(w);
		refreshImagePreviewSize();
	}

	public void decPreviewSize() {
		int w = config.getPreviewWidthSize()-10;
		if (w<1) {
			w = 1;
		}
		config.setPreviewWidthSize(w);
		refreshImagePreviewSize();
	}

	@Override
	public void onZoomChange(int arg0, boolean arg1, Camera arg2) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		//		log.v(this, "onPreviewFrame()");
		bSnapShot = data;
	}

	@Override
	public void onAutoFocus(boolean success, Camera camera) {
		log.v(this, "onAutoFocus(success:"+success+")");
//		if (camera!=null) {
			if (bSnapShot==null) {
				showToast(true, Toast.LENGTH_SHORT, "Image data not found");
				log.w(this, "Image data not found");
				setState(STATE_IDLE);
			} else if (cameraParameters==null) {
				showToast(true, Toast.LENGTH_SHORT, "Image parameter not found");
				log.w(this, "Image parameter not found");
				setState(STATE_IDLE);
			} else if (bSnapShot!=null) {
				long completeAutofocus = System.currentTimeMillis();
				synchronized (camera) {
					int delay = config.getAutoCaptureDelay();
					
//					log.d(this, "lastCapture : " + lastCapture + "|" + avgAutofocusTime);
					long different = System.currentTimeMillis()-lastCapture;
					try {
						if ((different>=delay || state==STATE_IMAGE_SINGLE) && !isTakingPicture) {
							//average autofocus speed
							if (avgAutofocusTime==-1) {
								avgAutofocusTime = completeAutofocus-lastStartAutofocus;
							} else {
								avgAutofocusTime += completeAutofocus-lastStartAutofocus;
								avgAutofocusTime /= 2;
							}
							log.d(this, "Average Focus Time : " + avgAutofocusTime);

							if (isImageHighRes) {
//								mute();
								isTakingPicture = true;
								log.d(this, "Calling takePicture");
								camera.takePicture(this, null, null, this);
								lastCapture = System.currentTimeMillis();
							} else {
								saveImage(true);
							}
						} else {
							if (isTakingPicture ) {
								log.w(this, "Ignoring the capture request because still in middle taking picture");
							} else {
								log.w(this, "Ignoring the capture request because different time in snapshot is " + different + " (delay: "+delay+")");
							}
						}

						if (state == STATE_IMAGE_AUTO) {
							long sleepTime = (delay-(System.currentTimeMillis()-lastCapture)-avgAutofocusTime);
							if (sleepTime<0) {
								sleepTime = 0;
							}
							new AutoFocusAsync().execute(sleepTime);
						} else if (state == STATE_IMAGE_SINGLE || state==STATE_IMAGE_FACE) {
							if (isCameraConfigure) {
								try {
									camera.cancelAutoFocus();
								} catch (RuntimeException e){
								}
							}
							if (state==STATE_IMAGE_SINGLE) {
								state = STATE_IDLE;
							}
						}
					} catch (IOException e) {
						log.w(this,e);
						if (isCameraConfigure) {
							try {
								camera.cancelAutoFocus();
							} catch (RuntimeException e2){
							}
						}
					}
				}
			}
//		} else {
//			state = STATE_IDLE;
//			showToast(false, Toast.LENGTH_SHORT, "Autofocus failed. Try to disabling autofocus in setting.");
//		}
	}

//	private void mute() {
//		log.d(this, "muting..");
//		AudioManager manager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
//		volSystem = manager.getStreamVolume(AudioManager.STREAM_SYSTEM);
//		manager.setStreamVolume(AudioManager.STREAM_SYSTEM, 0 , AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
//		manager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//	}
//
//	private void unmute() {
//		log.d(this, "unmuting..");
//		AudioManager manager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
//		manager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
//		manager.setStreamVolume(AudioManager.STREAM_SYSTEM, volSystem, AudioManager.FLAG_ALLOW_RINGER_MODES);
//	}
	
	private void saveImage(boolean yuv) throws IOException {
		log.d(this, "Calling saveImage");
		FileOutputStream filecon = null;
		try { 
			File directory = new File(config.getSavingPath());
			if (!directory.exists()) {
				directory.mkdir();
			}
			File file = new File( directory.getAbsolutePath()+ "/SpyPhoto_"+ SDF.format(new Date())+".jpg");
			filecon = new FileOutputStream(file);

			//Create Image from preview data - byte[]
			// converting to RGB for rotation
			int[] rgbData = null;
			if (yuv) {
				int[] imageRotate = new int[2];
				if (defaultOrientation==ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
					imageRotate[0] = 90;
					imageRotate[1] = -90;
				} else if (defaultOrientation==ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
					imageRotate[0] = 0;
					imageRotate[1] = 0;
				} else if (defaultOrientation==ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
					imageRotate[0] = 180;
					imageRotate[1] = 180;
				} else if (defaultOrientation==ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
					imageRotate[0] = -90;
					imageRotate[1] = 90;
				}
				log.d(this, "Rotating image : " + imageRotate[cameraId]);
				try {
					//USING manual YuvDecode
					Size size = cameraParameters.getPreviewSize();
					rgbData = new int[size.width*size.height];
					decodeYUV(rgbData, bSnapShot, size.width, size.height);
					Bitmap bitmap = Bitmap.createBitmap(rgbData, size.width, size.height, Bitmap.Config.ARGB_8888);
					if (imageRotate[cameraId]!=0) {
						Matrix m = new Matrix();
						m.setRotate(imageRotate[cameraId], (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
						Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, size.width, size.height, m, false);
						bitmap.recycle();
						bitmap2.compress(CompressFormat.JPEG, 100, filecon);
						bitmap2.recycle();
					} else {
						bitmap.compress(CompressFormat.JPEG, 100, filecon);
						bitmap.recycle();
					}
					showToast(false, Toast.LENGTH_SHORT, file.getAbsolutePath() + " : " + bitmap.getWidth() + "x" + bitmap.getHeight());
				} catch (RuntimeException re) {
					log.w(this, re);
					//USING Android YuvImage
					Size size = cameraParameters.getPreviewSize();
					showToast(false, Toast.LENGTH_SHORT, file.getAbsolutePath() + " : " + size.width + "x" + size.height);
					YuvImage yuvImage = new YuvImage(bSnapShot, cameraParameters.getPreviewFormat(), 
							size.width, size.height, null);
					yuvImage.compressToJpeg(new Rect(0, 0, size.width, size.height), 100, filecon);
					filecon.close();
					filecon = null;
					if (imageRotate[cameraId]!=0) {
						Bitmap bMap = BitmapFactory.decodeFile(file.getAbsolutePath());
						Matrix mat = new Matrix();
						mat.postRotate(imageRotate[cameraId]);
						Bitmap bitmap2 = Bitmap.createBitmap(bMap, 0, 0,
								bMap.getWidth(), bMap.getHeight(), mat, true);
						filecon = new FileOutputStream(file);
						bitmap2.compress(CompressFormat.JPEG, 100, filecon);
					}
				}
			} else {
				//Image not rotated to prevent outofmemory issue
				filecon.write(bSnapShot);
				showToast(false, Toast.LENGTH_SHORT, file.getAbsolutePath() + " : " + cameraParameters.getPictureSize().width + "x" + cameraParameters.getPictureSize().height);
			}
			if (config.isVibrate()) {
				Vibrator v = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
				v.vibrate(config.getVibrateTime());
			}

			//add it media scanner for Gallery
//			MediaScannerConnection.scanFile(activity, new String[] {file.getAbsolutePath()}, 
//					new String[] {"image/jpeg"}, null);
			addFileToMediaScanner(file);

			lastCapture = System.currentTimeMillis();
		} catch (RuntimeException e) {
			Message msg = new Message();
			msg.what = MainHandler.WHAT_SHOW_FAILED_PROCESS;
			msg.obj = new FailedProcessData(e
					, "Failed saving image. Please try to change the image resolution in setting.\nSending the report will help me solve the issue."
					, "Send Report"
					, "OK", false, null);
			handler.sendMessage(msg);
		} finally {
			if (filecon!=null) {
				try {
					filecon.close();
				} catch (IOException e) {}
			}
		}
	}
	
	private void addFileToMediaScanner(File f) {
		log.v(this, "addFileToMediaScanner(f:"+f.getPath()+")");
	    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
	    Uri contentUri = Uri.fromFile(f);
	    mediaScanIntent.setData(contentUri);
	    activity.sendBroadcast(mediaScanIntent);
	}

	public void decodeYUV(int[] out, byte[] fg, int width, int height)
			throws NullPointerException, IllegalArgumentException {
		log.v(this, "decodeYUV(out:"+out+"|fg:"+(fg!=null?fg.length:null)+"|w:"+width+"|h:"+height+")");
		int sz = width * height;
		if (out == null)
			throw new NullPointerException("buffer out is null");
		if (out.length < sz)
			throw new IllegalArgumentException("buffer out size " + out.length
					+ " < minimum " + sz);
		if (fg==null)
			throw new NullPointerException("buffer 'fg' is null");

		if (fg.length < sz)
			throw new IllegalArgumentException("buffer fg size " + fg.length
					+ " < minimum " + sz);

		int i, j;
		int Y, Cr = 0, Cb = 0;
		for (j = 0; j < height; j++) {
			int pixPtr = j * width;
			final int jDiv2 = j >> 1;
			for (i = 0; i < width; i++) {
				Y = fg[pixPtr];
				if (Y < 0)
					Y += 255;
				if ((i & 0x1) != 1) {
					final int cOff = sz + jDiv2 * width + (i >> 1) * 2;
					Cb = fg[cOff];
					if (Cb < 0)
						Cb += 127;
					else
						Cb -= 128;
					Cr = fg[cOff + 1];
					if (Cr < 0)
						Cr += 127;
					else
						Cr -= 128;
				}
				int R = Y + Cr + (Cr >> 2) + (Cr >> 3) + (Cr >> 5);
				if (R < 0)
					R = 0;
				else if (R > 255)
					R = 255;
				int G = Y - (Cb >> 2) + (Cb >> 4) + (Cb >> 5) - (Cr >> 1)
						+ (Cr >> 3) + (Cr >> 4) + (Cr >> 5);
				if (G < 0)
					G = 0;
				else if (G > 255)
					G = 255;
				int B = Y + Cb + (Cb >> 1) + (Cb >> 2) + (Cb >> 6);
				if (B < 0)
					B = 0;
				else if (B > 255)
					B = 255;
				out[pixPtr++] = (0xff000000 + (B << 16) + (G << 8) + R);
			}
		}

	}

	@Override
	public void onShutter() {
		log.v(this, "onShutter()");
//		unmute();
	}

	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		log.v(this, "onPictureTaken(data"+(data!=null?data.length:null)+")");
		bSnapShot = data;
		try {
			saveImage(false);
			camera.startPreview();
			isTakingPicture = false;
		} catch (IOException e) {
			log.w(this, e);
		}
	}
	
	public void setState(int state) {
		if (state==this.state) {
			return;
		}
		if (isUIDisplayed) {
			Message msg =  new Message();
			msg.what = MainHandler.WHAT_SET_STATE_UI;
			msg.arg1 = this.state;
			msg.arg2 = state;
			this.state = state;
			handler.sendMessage(msg);
		}
	}

	class AutoFocusAsync extends AsyncTask<Long, Void, Void> {

		@Override
		protected Void doInBackground(Long... params) {
			try {
				log.v(this, "doInBackground(sleep:" + params[0] + ")");
				Thread.sleep(params[0]);
				while (isTakingPicture) {
					Thread.sleep(params[0]);
				}
				if (state==STATE_IMAGE_AUTO) {
					startAutoFocus();
				}
			} catch (InterruptedException e) {}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}

	}

	public void blackScreenClick() {
		if (state==STATE_IDLE) {
			imageCapture();
		}
	}

	public boolean onScalePreview(ScaleGestureDetector sgd) {
		log.v(this, "onScale " + sgd.getScaleFactor() + "-" + cameraParameters.getMaxZoom());
		if (cameraParameters.isZoomSupported() || cameraParameters.isSmoothZoomSupported()) {
			if (sgd.getScaleFactor()>1) {
				if (cameraParameters.getZoom()<cameraParameters.getMaxZoom()) {
					zoomCurrent = cameraParameters.getZoom()+1;
					zoomRunning = true;
					if (cameraParameters.isSmoothZoomSupported()) {
						if (!zoomRunning) {
							camera.startSmoothZoom(zoomCurrent);
						}
					} else {
						cameraParameters.setZoom(zoomCurrent);
						camera.setParameters(cameraParameters);
					}
					log.i(this, "Zoom to : " + zoomCurrent);
				}
			} else if (sgd.getScaleFactor()<1) {
				if (cameraParameters.getZoom()>0) {
					zoomCurrent=cameraParameters.getZoom()-1;
					zoomRunning = true;
					if (cameraParameters.isSmoothZoomSupported()) {
						if (!zoomRunning) {
							camera.startSmoothZoom(zoomCurrent);
						}
					} else {
						cameraParameters.setZoom(zoomCurrent);
						camera.setParameters(cameraParameters);
					}
					log.i(this, "Zoom to : " + zoomCurrent);
				}
			}
		} else {
			if (!isZoomErrorHaveDisplayed) {
				isZoomErrorHaveDisplayed = true;
				showToast(true, Toast.LENGTH_SHORT, "Zoom not supported");
			}
		}
		return true;
	}
	
	private boolean checkPreviousCrash() {
		boolean justCrashed = config.isCrashed();
		if (!justCrashed) {
			return false;
		}
		final File file = new File(config.getCrashLogFilePath());
		if (!file.exists()) {
			showToast(true, Toast.LENGTH_LONG, "An error was detected but no report generated");
			return false;
		}
		
		File fileType = new File(config.getCrashTypeFilePath());
		int type = 0;
		if (fileType.exists()) {
			FileInputStream fis;
			try {
				fis = new FileInputStream(fileType);
				type = fis.read();
			} catch (FileNotFoundException e) {
			} catch (IOException e) {
			}
		}
		
		if (type==1) {
			config.setOutOfMemoryIssue();
		}

		Message msg = new Message();
		msg.what = MainHandler.WHAT_SHOW_CRASH_DIALOG;
		msg.arg1 = type;
		handler.sendMessage(msg);
		return true;
	}
	
	public String errorReport() {
		StringBuffer info = new StringBuffer();
		info.append("cameraId: " + this.cameraId + "\n");
		info.append("defaultOrientation: " + this.defaultOrientation + "\n");
		info.append("zoomCurrent: " + this.zoomCurrent + "\n");
		info.append("isHolderReady: " + this.isHolderReady + "\n");
		info.append("isCameraConfigure: " + this.isCameraConfigure + "\n");
		info.append("isImageHighRes: " + this.isImageHighRes + "\n");
		info.append("isBlackScreen: " + this.isBlackScreen + "\n");
		info.append("isTakingPicture: " + this.isTakingPicture + "\n");
		info.append("state: " + this.state + "\n");
		info.append("previewSize: ");
		if (this.previewSize!=null) {
			info.append(this.previewSize.width + "x" + this.previewSize.height + "\n");
		} else {
			info.append("null\n");
		}
		info.append("captureSize: ");
		if (this.captureSize!=null) {
			info.append(this.captureSize.width + "x" + this.captureSize.height + "\n");
		} else {
			info.append("null\n");
		}
		info.append("cameraParameters: ");
		try {
			Camera.Parameters temp = camera.getParameters();
			if (temp!=null) {
				this.cameraParameters = temp;
			}
		} catch (RuntimeException e) {}
		if (this.cameraParameters!=null) {
			info.append("\n" + "-getFocusMode(): " + this.cameraParameters.getFocusMode());
			info.append("\n" + "-getPreviewSize: ");
			if (this.cameraParameters.getPreviewSize()==null) {
				info.append("null");
			} else {
				info.append(this.cameraParameters.getPreviewSize().width + "x" + this.cameraParameters.getPreviewSize().height);
			}
			info.append("\n" + "-getPreviewFormat: " + this.cameraParameters.getPreviewFormat());
			info.append("\n" + "-getSupportedPreviewFormats(): ");
			for (Integer i : this.cameraParameters.getSupportedPreviewFormats()) {
				info.append(i+",");
			}
			info.append("\n" + "-getSupportedPreviewSizes(): ");
			for (Camera.Size i : this.cameraParameters.getSupportedPreviewSizes()) {
				info.append(i.width+"x"+i.height+",");
			}
			info.append("\n" + "-getPictureSize: " + this.cameraParameters.getPictureSize().width + "x" + this.cameraParameters.getPictureSize().height);
			info.append("\n" + "-getPictureFormat: " + this.cameraParameters.getPictureFormat());
			info.append("\n" + "-getSupportedPictureFormats(): ");
			for (Integer i : this.cameraParameters.getSupportedPictureFormats()) {
				info.append(i+",");
			}
			info.append("\n" + "-getSupportedPictureSizes(): ");
			for (Camera.Size i : this.cameraParameters.getSupportedPictureSizes()) {
				info.append(i.width+"x"+i.height+",");
			}
		} else {
			info.append("null\n");
		}
		info.append("\ncurrent video-quality: " + this.currentCamcorderProfileId + "\n");
		info.append("\nvideo-qualities back-cam: ");
		if (this.videoQualities!=null && this.videoQualities[0]!=null) {
			for (String quality : videoQualities[0]) {
				info.append(quality + ", ");
			}
		} else {
			info.append("\nnull\n");
		}
		info.append("\nvideo-qualities front-cam: ");
		if (this.videoQualities!=null && this.videoQualities.length>=2 && this.videoQualities[1]!=null) {
			for (String quality : videoQualities[1]) {
				info.append(quality + ", ");
			}
		} else {
			info.append("\nnull\n");
		}
		return info.toString();
	}

	public void forceOrientation() {
		log.v(this, "forceOrientation() to " + defaultOrientation);
		activity.setRequestedOrientation(defaultOrientation);
	}

	public void sendEmailCrash() {
		final File file = new File(config.getCrashLogFilePath());
		CrashHandler.getInstance(null, null).sendEmail(file);
	}

	public boolean pressVolumeDown() {
		log.v(this, "pressVolumeDown()");
		String type = config.getVolumeDownAction();
		if (type.equals("capture")) {
			imageCapture();
			return true;
		} else if (type.equals("auto")) {
			autoImageCapture();
			return true;
		} else if (type.equals("face")) {
			autoFaceCapture();
			return true;
		} else if (type.equals("video")) {
			videoRecording();
			return true;
		}
		return false;
	}

	public boolean pressVolumeUp() {
		log.v(this, "pressVolumeUp()");
		String type = config.getVolumeUpAction();
		if (type.equals("capture")) {
			imageCapture();
			return true;
		} else if (type.equals("auto")) {
			autoImageCapture();
			return true;
		} else if (type.equals("face")) {
			autoFaceCapture();
			return true;
		} else if (type.equals("video")) {
			videoRecording();
			return true;
		}
		return false;
	}

	public void uiResume(SurfaceView svPreview) {
		isUIDisplayed = true;
		ringerMode = Utility.getSound(activity);
		Utility.setSound(activity, AudioManager.RINGER_MODE_SILENT);
		startCamera(svPreview);
		config.reset();
		if (config.isLogging()) {
			log.enableLogging(activity);
		} else {
			log.disableLogging();
		}
		if (isBlackScreen && config.isShowToast()) {
			showToast(true, Toast.LENGTH_SHORT, activity.getString(R.string.hint_blackmode));
		}
	}

	public void uiPause() {
		isUIDisplayed = false;
		stopCamera();
		Utility.setSound(activity, ringerMode);
		isHolderReady = false;
	}

	public boolean pressBack() {
		if (isBlackScreen) {
			config.setPreviewWidthSize(tempPreviewWidth);
			activity.finish();
			return true;
		}
		return false;
		
	}

	public boolean pressMenu() {
		if (isBlackScreen) {
			switchBlackScreen();
			return true;
		}
		return false;
	}

	public boolean onScaleBlackScreen(ScaleGestureDetector sgd) {
		if (sgd.getScaleFactor()<1 && isBlackScreen) {
			switchBlackScreen();
		}
		return true;
	}
	
}
